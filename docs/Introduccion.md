# Introducción

## Terminología

Normalmente puede utilizarse **VM** de "Virtual Machine" en ingĺés, para referirse a una máquina virtual.

Se diferencian dos tipos de máquinas:

![](imagenes/Anfitrion_Huesped.jpg)

![](imagenes/Appliance_Hypervisor.jpg)

## Recursos

A cada máquina virtual se le pueden asignar parte de los recursos con los que cuenta el anfitrión. Por ejemplo:

![](imagenes/Ej_Recursos.jpg)
