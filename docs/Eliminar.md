# Clonar una máquina virtual

Para eliminar una Máquina Virtual hacer click con el botón derecho sobre el anfitrión en cuestion.
Luego seleccionar si se quiere eliminar sólo la configuración o la configuración y todos los archivos generados:

![](imagenes/Eliminar.jpg)
